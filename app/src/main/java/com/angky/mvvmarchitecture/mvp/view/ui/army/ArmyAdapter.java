package com.angky.mvvmarchitecture.mvp.view.ui.army;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.angky.mvvmarchitecture.R;
import com.angky.mvvmarchitecture.mvp.model.Army;

import java.util.List;

/**
 * Created by pegipegi on 12/14/2017.
 *
 */

public class ArmyAdapter extends RecyclerView.Adapter<ArmyViewHolder> {

    private List<Army> data;

    public ArmyAdapter(List<Army> data) {
        this.data = data;
    }

    @Override
    public ArmyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_army, parent, false);
        return new ArmyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ArmyViewHolder holder, int position) {
        Army army = data.get(position);
        holder.getmTextViewName().setText(army.getName());
        holder.getmTextViewWeapon().setText(army.getWeapon());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
