package com.angky.mvvmarchitecture.mvp.data.service.army;

import com.angky.mvvmarchitecture.mvp.data.dao.army.ArmyDao;
import com.angky.mvvmarchitecture.mvp.listener.OnDataArmyLoaded;
import com.angky.mvvmarchitecture.mvp.model.Army;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by pegipegi on 12/12/2017.
 *
 */

public class ArmyServiceImpl implements ArmyService, OnDataArmyLoaded {

    private ArmyDao dao;
    private OnDataArmyLoaded onDataArmyLoaded;

    @Inject
    public ArmyServiceImpl(ArmyDao dao) {
        this.dao = dao;
    }

    @Override
    public void loadData(OnDataArmyLoaded onDataArmyLoaded) {
        this.onDataArmyLoaded = onDataArmyLoaded;
        dao.loadData(this);
    }

    @Override
    public void onLoaded(List<Army> data) {
        this.onDataArmyLoaded.onLoaded(data);
    }

}
