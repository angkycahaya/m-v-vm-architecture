package com.angky.mvvmarchitecture.mvp.view.ui.player;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.angky.mvvmarchitecture.R;
import com.angky.mvvmarchitecture.mvp.model.Player;
import com.angky.mvvmarchitecture.mvp.view.ui.player.PlayerViewHolder;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 1/5/2018.
 */

public class PlayerAdapter extends RecyclerView.Adapter<PlayerViewHolder> {

    private List<Player> data;

    public PlayerAdapter(List<Player> data) {
        this.data = data;
    }

    @Override
    public PlayerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_player, parent, false);
        return new PlayerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PlayerViewHolder holder, int position) {
        Player player = data.get(position);
        holder.getmTextViewFullName().setText(player.getFullName());
        holder.getmTextViewNickName().setText(player.getNickName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
