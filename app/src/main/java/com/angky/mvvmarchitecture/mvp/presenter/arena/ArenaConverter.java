package com.angky.mvvmarchitecture.mvp.presenter.arena;

import com.angky.mvvmarchitecture.mvp.model.Arena;
import com.angky.mvvmarchitecture.mvp.model.ArenaApi;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by developmentpraisindo on 1/14/18.
 */

public class ArenaConverter {

    public static Arena convertApiToDefault(ArenaApi arenaApi) {
        if (arenaApi != null) {
            Arena arena = new Arena();
            arena.setId(arenaApi.getCode());
            arena.setName(arenaApi.getPlace());
            return arena;
        } else {
            return null;
        }
    }

    public static Arena getArenaById(List<ArenaApi> arenas, int id) {
        Arena arena = null;
        for (ArenaApi arenaApi : arenas) {
            if (arenaApi.getCode() == id) {
                arena = ArenaConverter.convertApiToDefault(arenaApi);
            }
        }
        return arena;
    }

    public static List<Arena> convertListApiToDefault(List<ArenaApi> arenaApis) {
        List<Arena> arenas = new ArrayList<>();
        for (ArenaApi arenaApi : arenaApis) {
            arenas.add(convertApiToDefault(arenaApi));
        }
        return arenas;
    }

}
