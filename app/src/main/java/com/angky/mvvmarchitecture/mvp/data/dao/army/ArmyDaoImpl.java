package com.angky.mvvmarchitecture.mvp.data.dao.army;

import com.angky.mvvmarchitecture.mvp.listener.OnDataArmyLoaded;
import com.angky.mvvmarchitecture.mvp.model.Army;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pegipegi on 12/12/2017.
 */

public class ArmyDaoImpl implements ArmyDao {

    private List<Army> data;
    private OnDataArmyLoaded onDataArmyLoaded;

    @Override
    public void loadData(OnDataArmyLoaded onDataArmyLoaded) {
        this.onDataArmyLoaded = onDataArmyLoaded;
        loadDataFromAPI();
    }

    private void loadDataFromAPI() {
        LoadDataFromAPI loadDataFromAPI = new LoadDataFromAPI();
        Thread thread = new Thread(loadDataFromAPI);
        thread.start();
    }

    private class LoadDataFromAPI extends Thread {

        @Override
        public void run() {
            //lets say we already call an API and get the data
            Army data1 = new Army(1, "Maverick", "Maverick M4A1 Carbine");
            Army data2 = new Army(2, "John", "Magnum Sniper Rifle");
            Army data3 = new Army(3, "Jake", "Bullpup");
            data = new ArrayList<>();
            data.add(data1);
            data.add(data2);
            data.add(data3);
            try {
                Thread.sleep(3000);
            } catch (Exception ex) {

            }
            onDataArmyLoaded.onLoaded(data);
        }

    }
}
