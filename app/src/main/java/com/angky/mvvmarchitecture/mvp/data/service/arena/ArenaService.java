package com.angky.mvvmarchitecture.mvp.data.service.arena;

import com.angky.mvvmarchitecture.mvp.model.ArenaApi;

import java.util.List;

/**
 * Created by developmentpraisindo on 1/14/18.
 */

public interface ArenaService {

    public List<ArenaApi> getData();
}
