package com.angky.mvvmarchitecture.mvp.view.ui.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.angky.mvvmarchitecture.R;
import com.angky.mvvmarchitecture.mvp.view.ui.arena.ArenaActivity;
import com.angky.mvvmarchitecture.mvp.view.ui.army.ArmyActivity;
import com.angky.mvvmarchitecture.mvp.view.ui.building.BuildingActivity;
import com.angky.mvvmarchitecture.mvp.view.ui.player.PlayerActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;

public class MainActivity extends AppCompatActivity implements MainUseCase {

    @BindView(R.id.button_army)
    Button mButtonMVPArmy;
    @BindView(R.id.button_players)
    Button mButtonMVPPlayers;
    @BindView(R.id.button_arenas)
    Button mButtonMVPArenas;
    @BindView(R.id.button_building)
    Button mButtonMVPBuilding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_army)
    public void onButtonMVPArmyClicked() {
        ArmyActivity.startActivity(this);
    }

    @OnClick(R.id.button_players)
    public void onButtonMVPPlayersClicked() {
        PlayerActivity.startActivity(this);
    }

    @OnClick(R.id.button_arenas)
    public void onButtonMVPArenasClicked() {
        ArenaActivity.startActivity(this);
    }

    @OnClick(R.id.button_building)
    public void onButtonMVPBuildingClicked() {
        BuildingActivity.startActivity(this);
    }
}
