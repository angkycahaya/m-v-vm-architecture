package com.angky.mvvmarchitecture.mvp.presenter.building;

/**
 * Created by PEGIPEGI ROG on 1/16/2018.
 */

public interface BuildingPresenter {

    public void loadData();
}
