package com.angky.mvvmarchitecture.mvp.injector;

import android.app.Application;

import com.angky.mvvmarchitecture.mvp.app.MVPArchitectureApplication;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * Created by PEGIPEGI ROG on 1/2/2018.
 */

@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    void inject(MVPArchitectureApplication app);

}
