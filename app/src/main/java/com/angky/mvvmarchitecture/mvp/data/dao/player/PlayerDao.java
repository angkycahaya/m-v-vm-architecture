package com.angky.mvvmarchitecture.mvp.data.dao.player;

import com.angky.mvvmarchitecture.mvp.listener.OnDataPlayerLoaded;

/**
 * Created by PEGIPEGI ROG on 1/5/2018.
 */

public interface PlayerDao {

    public void loadData(OnDataPlayerLoaded onDataPlayerLoaded);
}
