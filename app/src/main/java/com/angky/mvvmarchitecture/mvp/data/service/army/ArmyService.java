package com.angky.mvvmarchitecture.mvp.data.service.army;

import com.angky.mvvmarchitecture.mvp.listener.OnDataArmyLoaded;

/**
 * Created by pegipegi on 12/12/2017.
 *
 */

public interface ArmyService {

    public void loadData(OnDataArmyLoaded onDataArmyLoaded);
}
