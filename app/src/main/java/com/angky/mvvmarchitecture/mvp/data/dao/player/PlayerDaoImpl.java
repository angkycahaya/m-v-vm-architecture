package com.angky.mvvmarchitecture.mvp.data.dao.player;

import com.angky.mvvmarchitecture.mvp.listener.OnDataPlayerLoaded;
import com.angky.mvvmarchitecture.mvp.model.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PEGIPEGI ROG on 1/5/2018.
 */

public class PlayerDaoImpl implements PlayerDao {

    private List<Player> data;
    private OnDataPlayerLoaded onDataPlayerLoaded;

    @Override
    public void loadData(OnDataPlayerLoaded onDataPlayerLoaded) {
        this.onDataPlayerLoaded = onDataPlayerLoaded;
        loadDataFromAPI();
    }

    private void loadDataFromAPI() {
        LoadDataFromAPI loadDataFromAPI = new LoadDataFromAPI();
        Thread thread = new Thread(loadDataFromAPI);
        thread.start();
    }

    private class LoadDataFromAPI extends Thread {

        @Override
        public void run() {
            //lets say we already call an API and get the data
            Player data1 = new Player(1, "Dono", "Mr. D");
            Player data2 = new Player(2, "Casino", "Mr. C");
            Player data3 = new Player(3, "Indro", "Mr. I");
            data = new ArrayList<>();
            data.add(data1);
            data.add(data2);
            data.add(data3);
            try {
                Thread.sleep(3000);
            } catch (Exception ex) {

            }
            onDataPlayerLoaded.onLoaded(data);
        }

    }
}
