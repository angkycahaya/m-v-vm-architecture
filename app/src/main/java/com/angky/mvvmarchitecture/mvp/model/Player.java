package com.angky.mvvmarchitecture.mvp.model;

/**
 * Created by PEGIPEGI ROG on 1/5/2018.
 */

public class Player {

    private int id;
    private String fullName;
    private String nickName;

    public Player(int id, String fullName, String nickName) {
        this.id = id;
        this.fullName = fullName;
        this.nickName = nickName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
