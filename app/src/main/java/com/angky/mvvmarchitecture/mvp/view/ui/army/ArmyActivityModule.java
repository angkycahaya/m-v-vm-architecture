package com.angky.mvvmarchitecture.mvp.view.ui.army;

import com.angky.mvvmarchitecture.mvp.data.dao.army.ArmyDao;
import com.angky.mvvmarchitecture.mvp.data.dao.army.ArmyDaoImpl;
import com.angky.mvvmarchitecture.mvp.data.service.army.ArmyService;
import com.angky.mvvmarchitecture.mvp.data.service.army.ArmyServiceImpl;
import com.angky.mvvmarchitecture.mvp.presenter.army.ArmyPresenter;
import com.angky.mvvmarchitecture.mvp.presenter.army.ArmyPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by developmentpraisindo on 1/22/18.
 */

@Module
public class ArmyActivityModule {

    @Provides
    ArmyDao provideArmyDao() {
        return new ArmyDaoImpl();
    }

    @Provides
    ArmyService provideArmyService(ArmyDao armyDao) {
        return new ArmyServiceImpl(armyDao);
    }

    @Provides
    ArmyPresenter provideArmyPresenter(ArmyUseCase armyUseCase, ArmyService armyService) {
        return new ArmyPresenterImpl(armyUseCase, armyService);
    }

    @Provides
    ArmyUseCase provideArmyUseCase(ArmyActivity armyActivity) {
        return armyActivity;
    }

}
