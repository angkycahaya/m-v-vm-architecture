package com.angky.mvvmarchitecture.mvp.app;

import android.app.Activity;
import android.app.Application;

import com.angky.mvvmarchitecture.mvp.injector.DaggerAppComponent;

import javax.inject.Inject;

import dagger.Module;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * Created by pegipegi on 12/12/2017.
 *
 */

@Module
public class MVPArchitectureApplication extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerAppComponent
                .builder()
                .application(this)
                .build()
                .inject(this);
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }
}
