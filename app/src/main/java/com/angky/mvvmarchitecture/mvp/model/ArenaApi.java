package com.angky.mvvmarchitecture.mvp.model;

/**
 * Created by developmentpraisindo on 1/14/18.
 */

public class ArenaApi {

    private int code;
    private String place;

    public ArenaApi() {
    }

    public ArenaApi(int code, String place) {
        this.code = code;
        this.place = place;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

}
