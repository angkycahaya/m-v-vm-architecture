package com.angky.mvvmarchitecture.mvp.view.ui.player;

import com.angky.mvvmarchitecture.mvp.model.Player;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 1/5/2018.
 */

public interface PlayerUseCase {

    public PlayerActivity getActivity();

    public void showStatusView(int viewStatus);

    public void showData(List<Player> data);
}
