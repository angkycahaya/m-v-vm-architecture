package com.angky.mvvmarchitecture.mvp.model;

/**
 * Created by PEGIPEGI ROG on 1/16/2018.
 */

public class Building {

    private int id;
    private String name;
    private String high;
    private Arena arena;

    public Building() {
    }

    public Building(int id, String name, String high, Arena arena) {
        this.id = id;
        this.name = name;
        this.high = high;
        this.arena = arena;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public Arena getArena() {
        return arena;
    }

    public void setArena(Arena arena) {
        this.arena = arena;
    }
}
