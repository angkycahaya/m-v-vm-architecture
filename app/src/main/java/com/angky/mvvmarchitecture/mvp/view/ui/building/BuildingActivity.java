package com.angky.mvvmarchitecture.mvp.view.ui.building;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.angky.mvvmarchitecture.R;
import com.angky.mvvmarchitecture.mvp.model.Building;
import com.angky.mvvmarchitecture.mvp.presenter.building.BuildingPresenter;
import com.angky.mvvmarchitecture.mvp.view.ConstantView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

/**
 * Created by PEGIPEGI ROG on 1/16/2018.
 */

public class BuildingActivity extends Activity implements BuildingUseCase {

    @BindView(R.id.progress_bar_building)
    ProgressBar mProgressBar;
    @BindView(R.id.linear_layout_building)
    LinearLayout mLinearLayoutBuilding;
    @BindView(R.id.text_view_result)
    TextView mTextViewResult;
    @BindView(R.id.recycler_view_building)
    RecyclerView mRecyclerViewBuilding;

    private BuildingPresenter mBuildingPresenter;
    private List<Building> mData;
    private BuildingAdapter mBuildingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_building);
        ButterKnife.bind(this);
        init();
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, BuildingActivity.class);
        context.startActivity(intent);
    }

    @Inject
    public void setmBuildingPresenter(BuildingPresenter mBuildingPresenter) {
        this.mBuildingPresenter = mBuildingPresenter;
    }

    private void init() {
        initAdapter();
        loadData();
    }

    private void initAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerViewBuilding.setLayoutManager(linearLayoutManager);
        mData = new ArrayList<>();
        mBuildingAdapter = new BuildingAdapter(mData);
        mRecyclerViewBuilding.setAdapter(mBuildingAdapter);
    }

    private void loadData() {
        mBuildingPresenter.loadData();
    }

    @Override
    public void showStatusView(int viewStatus) {
        switch (viewStatus) {
            case ConstantView.VIEW_LOADING:
                mLinearLayoutBuilding.setVisibility(View.GONE);
                mTextViewResult.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
                break;
            case ConstantView.VIEW_SUCCESS:
                mLinearLayoutBuilding.setVisibility(View.VISIBLE);
                mTextViewResult.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.GONE);
                break;
            case ConstantView.VIEW_EMPTY:
                mLinearLayoutBuilding.setVisibility(View.GONE);
                mTextViewResult.setText("The Squad List is Empty");
                mTextViewResult.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
                break;
            case ConstantView.VIEW_ERROR:
                mLinearLayoutBuilding.setVisibility(View.GONE);
                mTextViewResult.setText("We have tried our best, sorry");
                mTextViewResult.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public void showData(List<Building> data) {
        this.mData.clear();
        this.mData.addAll(data);
        mBuildingAdapter.notifyDataSetChanged();
    }
}