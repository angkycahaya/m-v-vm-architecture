package com.angky.mvvmarchitecture.mvp.data.service.player;

import com.angky.mvvmarchitecture.mvp.data.dao.player.PlayerDao;
import com.angky.mvvmarchitecture.mvp.listener.OnDataPlayerLoaded;
import com.angky.mvvmarchitecture.mvp.model.Player;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by PEGIPEGI ROG on 1/5/2018.
 */

public class PlayerServiceImpl implements PlayerService, OnDataPlayerLoaded {

    private PlayerDao dao;
    private OnDataPlayerLoaded onDataPlayerLoaded;

    @Inject
    public PlayerServiceImpl(PlayerDao dao) {
        this.dao = dao;
    }

    @Override
    public void loadData(OnDataPlayerLoaded onDataPlayerLoaded) {
        this.onDataPlayerLoaded = onDataPlayerLoaded;
        dao.loadData(this);
    }

    @Override
    public void onLoaded(List<Player> data) {
        this.onDataPlayerLoaded.onLoaded(data);
    }

}
