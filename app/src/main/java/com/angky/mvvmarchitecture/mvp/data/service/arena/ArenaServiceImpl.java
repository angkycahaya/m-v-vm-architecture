package com.angky.mvvmarchitecture.mvp.data.service.arena;

import com.angky.mvvmarchitecture.mvp.data.dao.arena.ArenaDao;
import com.angky.mvvmarchitecture.mvp.model.ArenaApi;

import java.util.List;

/**
 * Created by developmentpraisindo on 1/14/18.
 */

public class ArenaServiceImpl implements ArenaService {

    private ArenaDao arenaDao;

    public ArenaServiceImpl(ArenaDao arenaDao) {
        this.arenaDao = arenaDao;
    }

    @Override
    public List<ArenaApi> getData() {
        return arenaDao.getData();
    }
}
