package com.angky.mvvmarchitecture.mvp.view.ui.building;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.angky.mvvmarchitecture.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by PEGIPEGI ROG on 1/16/2018.
 */

public class BuildingViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text_view_arena)
    TextView mTextViewArena;
    @BindView(R.id.text_view_name)
    TextView mTextViewName;
    @BindView(R.id.text_view_high)
    TextView mTextViewHigh;

    public BuildingViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public TextView getmTextViewArena() {
        return mTextViewArena;
    }

    public TextView getmTextViewName() {
        return mTextViewName;
    }

    public TextView getmTextViewHigh() {
        return mTextViewHigh;
    }
}
