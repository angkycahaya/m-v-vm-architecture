package com.angky.mvvmarchitecture.mvp.listener;

import com.angky.mvvmarchitecture.mvp.model.Army;

import java.util.List;

/**
 * Created by pegipegi on 12/13/2017.
 *
 */

public interface OnDataArmyLoaded {

    public void onLoaded(List<Army> data);

}
