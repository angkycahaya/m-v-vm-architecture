package com.angky.mvvmarchitecture.mvp.view.ui.building;

import com.angky.mvvmarchitecture.mvp.data.dao.arena.ArenaDao;
import com.angky.mvvmarchitecture.mvp.data.dao.arena.ArenaDaoImpl;
import com.angky.mvvmarchitecture.mvp.data.dao.building.BuildingDao;
import com.angky.mvvmarchitecture.mvp.data.dao.building.BuildingDaoImpl;
import com.angky.mvvmarchitecture.mvp.data.service.arena.ArenaService;
import com.angky.mvvmarchitecture.mvp.data.service.arena.ArenaServiceImpl;
import com.angky.mvvmarchitecture.mvp.data.service.building.BuildingService;
import com.angky.mvvmarchitecture.mvp.data.service.building.BuildingServiceImpl;
import com.angky.mvvmarchitecture.mvp.presenter.building.BuildingPresenter;
import com.angky.mvvmarchitecture.mvp.presenter.building.BuildingPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by developmentpraisindo on 1/22/18.
 */

@Module
public class BuildingActivityModule {

    @Provides
    ArenaDao provideArenaDao() {
        return new ArenaDaoImpl();
    }

    @Provides
    ArenaService proviedArenaService(ArenaDao arenaDao) {
        return new ArenaServiceImpl(arenaDao);
    }

    @Provides
    BuildingDao provideBuildingDao() {
        return new BuildingDaoImpl();
    }

    @Provides
    BuildingService proviedBuildingService(BuildingDao buildingDao) {
        return new BuildingServiceImpl(buildingDao);
    }

    @Provides
    BuildingPresenter provideBuildingPresenter(BuildingUseCase buildingUseCase, ArenaService arenaService, BuildingService buildingService) {
        return new BuildingPresenterImpl(buildingUseCase, arenaService, buildingService);
    }

    @Provides
    BuildingUseCase provideBuildingUseCase(BuildingActivity buildingActivity) {
        return buildingActivity;
    }
}
