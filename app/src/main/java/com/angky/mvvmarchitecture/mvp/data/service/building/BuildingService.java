package com.angky.mvvmarchitecture.mvp.data.service.building;

import com.angky.mvvmarchitecture.mvp.model.BuildingApi;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 1/16/2018.
 */

public interface BuildingService {

    public List<BuildingApi> getData();
}
