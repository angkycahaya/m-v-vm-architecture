package com.angky.mvvmarchitecture.mvp.view.ui.player;

import com.angky.mvvmarchitecture.mvp.data.dao.player.PlayerDao;
import com.angky.mvvmarchitecture.mvp.data.dao.player.PlayerDaoImpl;
import com.angky.mvvmarchitecture.mvp.data.service.player.PlayerService;
import com.angky.mvvmarchitecture.mvp.data.service.player.PlayerServiceImpl;
import com.angky.mvvmarchitecture.mvp.presenter.player.PlayerPresenter;
import com.angky.mvvmarchitecture.mvp.presenter.player.PlayerPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by developmentpraisindo on 1/22/18.
 */

@Module
public class PlayerActivityModule {

    @Provides
    PlayerDao providePlayerDao() {
        return new PlayerDaoImpl();
    }

    @Provides
    PlayerService providePlayerService(PlayerDao playerDao) {
        return new PlayerServiceImpl(playerDao);
    }

    @Provides
    PlayerPresenter providePlayerPresenter(PlayerUseCase playerUseCase, PlayerService playerService) {
        return new PlayerPresenterImpl(playerUseCase, playerService);
    }

    @Provides
    PlayerUseCase providePlayerUseCase(PlayerActivity playerActivity) {
        return playerActivity;
    }

}
