package com.angky.mvvmarchitecture.mvp.view.ui.building;

import com.angky.mvvmarchitecture.mvp.model.Building;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 1/16/2018.
 */

public interface BuildingUseCase {

    public void showStatusView(int viewStatus);

    public void showData(List<Building> data);
}
