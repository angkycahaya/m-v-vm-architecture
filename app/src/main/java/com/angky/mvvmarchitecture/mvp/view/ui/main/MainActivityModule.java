package com.angky.mvvmarchitecture.mvp.view.ui.main;

import dagger.Module;
import dagger.Provides;

/**
 * Created by PEGIPEGI ROG on 1/3/2018.
 */

@Module
public class MainActivityModule {

    @Provides
    MainUseCase provideMainUseCase(MainActivity mainActivity) {
        return mainActivity;
    }

}
