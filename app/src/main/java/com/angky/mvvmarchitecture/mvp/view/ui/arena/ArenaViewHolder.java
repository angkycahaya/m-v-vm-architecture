package com.angky.mvvmarchitecture.mvp.view.ui.army;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.angky.mvvmarchitecture.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pegipegi on 12/14/2017.
 *
 */

public class ArenaViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text_view_id)
    TextView mTextViewId;
    @BindView(R.id.text_view_name)
    TextView mTextViewName;

    public ArenaViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public TextView getmTextViewId() {
        return mTextViewId;
    }

    public TextView getmTextViewName() {
        return mTextViewName;
    }
}
