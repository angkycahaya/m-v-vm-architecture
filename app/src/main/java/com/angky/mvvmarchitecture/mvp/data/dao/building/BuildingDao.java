package com.angky.mvvmarchitecture.mvp.data.dao.building;

import com.angky.mvvmarchitecture.mvp.model.BuildingApi;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 1/16/2018.
 */

public interface BuildingDao {

    public List<BuildingApi> getData();
}
