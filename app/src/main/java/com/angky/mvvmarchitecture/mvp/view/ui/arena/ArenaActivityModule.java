package com.angky.mvvmarchitecture.mvp.view.ui.arena;

import com.angky.mvvmarchitecture.mvp.data.dao.arena.ArenaDao;
import com.angky.mvvmarchitecture.mvp.data.dao.arena.ArenaDaoImpl;
import com.angky.mvvmarchitecture.mvp.data.service.arena.ArenaService;
import com.angky.mvvmarchitecture.mvp.data.service.arena.ArenaServiceImpl;
import com.angky.mvvmarchitecture.mvp.presenter.arena.ArenaPresenter;
import com.angky.mvvmarchitecture.mvp.presenter.arena.ArenaPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by developmentpraisindo on 1/22/18.
 */

@Module
public class ArenaActivityModule {

    @Provides
    ArenaDao provideArenaDao() {
        return new ArenaDaoImpl();
    }

    @Provides
    ArenaService proviedArenaService(ArenaDao arenaDao) {
        return new ArenaServiceImpl(arenaDao);
    }

    @Provides
    ArenaPresenter provideArenaPresenter(ArenaUseCase arenaUseCase, ArenaService arenaService) {
        return new ArenaPresenterImpl(arenaUseCase, arenaService);
    }

    @Provides
    ArenaUseCase provideArenaUseCase(ArenaActivity arenaActivity) {
        return arenaActivity;
    }
}
