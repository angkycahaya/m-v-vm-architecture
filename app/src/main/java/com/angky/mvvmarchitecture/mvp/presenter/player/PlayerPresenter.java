package com.angky.mvvmarchitecture.mvp.presenter.player;

/**
 * Created by PEGIPEGI ROG on 1/5/2018.
 */

public interface PlayerPresenter {

    public void loadData();
}
