package com.angky.mvvmarchitecture.mvp.presenter.arena;

/**
 * Created by developmentpraisindo on 1/14/18.
 */

public interface ArenaPresenter {

    public void loadData();
}
