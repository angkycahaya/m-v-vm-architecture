package com.angky.mvvmarchitecture.mvp.presenter.army;

import android.util.Log;

import com.angky.mvvmarchitecture.mvp.data.service.army.ArmyService;
import com.angky.mvvmarchitecture.mvp.listener.OnDataArmyLoaded;
import com.angky.mvvmarchitecture.mvp.model.Army;
import com.angky.mvvmarchitecture.mvp.view.ConstantView;
import com.angky.mvvmarchitecture.mvp.view.ui.army.ArmyUseCase;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by pegipegi on 12/12/2017.
 */

public class ArmyPresenterImpl implements ArmyPresenter {

    private ArmyService armyService;
    private ArmyUseCase armyUseCase;

    @Inject
    public ArmyPresenterImpl(ArmyUseCase armyUseCase, ArmyService armyService) {
        this.armyUseCase = armyUseCase;
        this.armyService = armyService;
    }

    public void loadData() {
        armyService.loadData(getCallBack());
        armyUseCase.showStatusView(ConstantView.VIEW_LOADING);
    }

    private OnDataArmyLoaded getCallBack() {
        return new OnDataArmyLoaded() {
            @Override
            public void onLoaded(final List<Army> data) {
                armyUseCase.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (data != null) {
                            if (data.size() > 0) {
                                armyUseCase.showStatusView(ConstantView.VIEW_SUCCESS);
                                armyUseCase.showData(data);
                            } else {
                                armyUseCase.showStatusView(ConstantView.VIEW_EMPTY);
                            }
                        } else {
                            armyUseCase.showStatusView(ConstantView.VIEW_ERROR);
                        }
                    }
                });
            }
        };
    }

    @Override
    public void observeObservable() {
        getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getObservableObserver());
    }

    private Observable<String> getObservable() {
        return Observable.just("Turn Right", "Turn Left");
    }

    private Observer<String> getObservableObserver() {
        return new Observer<String>() {

            @Override
            public void onSubscribe(Disposable d) {
                Log.e("onSubscribe", "Subscribed");
            }

            @Override
            public void onNext(String s) {
                Log.e("onNext", s);
            }

            @Override
            public void onError(Throwable e) {
                Log.e("onError", e.getMessage());
            }

            @Override
            public void onComplete() {
                Log.e("onComplete", "Completed");
            }
        };
    }

    @Override
    public void observeFlowable() {
        /*getFlowable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getFlowableObserver());*/
    }

    private Flowable<String> getFlowable() {
        return Flowable.just("Turn Right", "Turn Left");
    }

    private Observer<String> getFlowableObserver() {
        return new Observer<String>() {

            @Override
            public void onSubscribe(Disposable d) {
                Log.e("onSubscribe", "Subscribed");
            }

            @Override
            public void onNext(String s) {
                Log.e("onNext", s);
            }

            @Override
            public void onError(Throwable e) {
                Log.e("onError", e.getMessage());
            }

            @Override
            public void onComplete() {
                Log.e("onComplete", "Completed");
            }
        };
    }

    @Override
    public void observeSingle() {
        getSingle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSingleObserver());
    }

    private Single<String> getSingle() {
        return Single.just("Turn Arround");
    }

    private SingleObserver<String> getSingleObserver() {
        return new SingleObserver<String>() {

            @Override
            public void onSubscribe(Disposable d) {
                Log.e("onSubscribe", "Subscribed");
            }

            @Override
            public void onSuccess(String s) {
                Log.e("onSuccess", s);
            }

            @Override
            public void onError(Throwable e) {
                Log.e("onError", e.getMessage());
            }
        };
    }

    @Override
    public void observeMaybe() {
        getMaybe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getMaybeObserver());
    }

    private Maybe<String> getMaybe() {
        return Maybe.just("Maybe Turn");
    }

    private MaybeObserver<String> getMaybeObserver() {
        return new MaybeObserver<String>() {

            @Override
            public void onSubscribe(Disposable d) {
                Log.e("onSubscribe", "Subscribed");
            }

            @Override
            public void onSuccess(String s) {
                Log.e("onSuccess", s);
            }

            @Override
            public void onError(Throwable e) {
                Log.e("onError", e.getMessage());
            }

            @Override
            public void onComplete() {
                Log.e("onComplete", "Completed");
            }
        };
    }

    @Override
    public void observeCompletable() {
        getCompletable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getCompletableObserver());
    }

    private Completable getCompletable() {
        return Completable.timer(1000, TimeUnit.MILLISECONDS);
    }

    private CompletableObserver getCompletableObserver() {
        return new CompletableObserver() {

            @Override
            public void onSubscribe(Disposable d) {
                Log.e("onSubscribe", "Subscribed");
            }

            @Override
            public void onError(Throwable e) {
                Log.e("onError", e.getMessage());
            }

            @Override
            public void onComplete() {
                Log.e("onComplete", "Completed");
            }
        };
    }
}
