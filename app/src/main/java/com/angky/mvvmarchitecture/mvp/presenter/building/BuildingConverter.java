package com.angky.mvvmarchitecture.mvp.presenter.building;

import com.angky.mvvmarchitecture.mvp.model.Arena;
import com.angky.mvvmarchitecture.mvp.model.ArenaApi;
import com.angky.mvvmarchitecture.mvp.model.Building;
import com.angky.mvvmarchitecture.mvp.model.BuildingApi;
import com.angky.mvvmarchitecture.mvp.presenter.arena.ArenaConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PEGIPEGI ROG on 1/16/2018.
 */

public class BuildingConverter {

    public static Building convertApiToDefault(List<ArenaApi> arenas, BuildingApi buildingApi) {
        if (buildingApi != null) {
            Building building = new Building();
            building.setId(buildingApi.getId());
            building.setName(buildingApi.getName());
            building.setHigh(buildingApi.getVolume());
            Arena arena = ArenaConverter.getArenaById(arenas, buildingApi.getArenaId());
            building.setArena(arena);
            return building;
        } else {
            return null;
        }
    }

    public static List<Building> convertBuildingArena(List<BuildingApi> buildings, List<ArenaApi> arenas) {
        List<Building> result = new ArrayList<>();
        for (BuildingApi buildingApi : buildings) {
            result.add(convertApiToDefault(arenas, buildingApi));
        }
        return result;
    }

}
