package com.angky.mvvmarchitecture.mvp.view.ui.arena;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.angky.mvvmarchitecture.R;
import com.angky.mvvmarchitecture.mvp.model.Arena;
import com.angky.mvvmarchitecture.mvp.view.ui.army.ArenaViewHolder;

import java.util.List;

/**
 * Created by pegipegi on 12/14/2017.
 *
 */

public class ArenaAdapter extends RecyclerView.Adapter<ArenaViewHolder> {

    private List<Arena> data;

    public ArenaAdapter(List<Arena> data) {
        this.data = data;
    }

    @Override
    public ArenaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_arena, parent, false);
        return new ArenaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ArenaViewHolder holder, int position) {
        Arena arena = data.get(position);
        holder.getmTextViewId().setText(Integer.toString(arena.getId()));
        holder.getmTextViewName().setText(arena.getName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
