package com.angky.mvvmarchitecture.mvp.presenter.player;

import com.angky.mvvmarchitecture.mvp.data.service.player.PlayerService;
import com.angky.mvvmarchitecture.mvp.listener.OnDataPlayerLoaded;
import com.angky.mvvmarchitecture.mvp.model.Player;
import com.angky.mvvmarchitecture.mvp.view.ConstantView;
import com.angky.mvvmarchitecture.mvp.view.ui.player.PlayerUseCase;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by PEGIPEGI ROG on 1/5/2018.
 */

public class PlayerPresenterImpl implements PlayerPresenter {

    private PlayerService playerService;
    private PlayerUseCase playerUseCase;

    @Inject
    public PlayerPresenterImpl(PlayerUseCase playerUseCase, PlayerService playerService) {
        this.playerUseCase = playerUseCase;
        this.playerService = playerService;
    }

    public void loadData() {
        playerService.loadData(getCallBack());
        playerUseCase.showStatusView(ConstantView.VIEW_LOADING);
    }

    private OnDataPlayerLoaded getCallBack() {
        return new OnDataPlayerLoaded() {
            @Override
            public void onLoaded(final List<Player> data) {
                playerUseCase.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (data != null) {
                            if (data.size() > 0) {
                                playerUseCase.showStatusView(ConstantView.VIEW_SUCCESS);
                                playerUseCase.showData(data);
                            } else {
                                playerUseCase.showStatusView(ConstantView.VIEW_EMPTY);
                            }
                        } else {
                            playerUseCase.showStatusView(ConstantView.VIEW_ERROR);
                        }
                    }
                });
            }
        };
    }

}
