package com.angky.mvvmarchitecture.mvp.view.ui.player;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.angky.mvvmarchitecture.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by PEGIPEGI ROG on 1/5/2018.
 */

public class PlayerViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text_view_full_name)
    TextView mTextViewFullName;
    @BindView(R.id.text_view_nick_name)
    TextView mTextViewNickName;

    public PlayerViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public TextView getmTextViewFullName() {
        return mTextViewFullName;
    }

    public TextView getmTextViewNickName() {
        return mTextViewNickName;
    }
}
