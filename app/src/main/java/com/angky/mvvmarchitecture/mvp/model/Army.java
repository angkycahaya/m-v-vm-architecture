package com.angky.mvvmarchitecture.mvp.model;

/**
 * Created by pegipegi on 12/12/2017.
 *
 */
public class Army {

    private int id;
    private String name;
    private String weapon;

    public Army(int id, String name, String weapon) {
        this.id = id;
        this.name = name;
        this.weapon = weapon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }
}
