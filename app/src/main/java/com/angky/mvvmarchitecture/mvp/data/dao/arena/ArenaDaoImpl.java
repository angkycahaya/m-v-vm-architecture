package com.angky.mvvmarchitecture.mvp.data.dao.arena;

import com.angky.mvvmarchitecture.mvp.model.ArenaApi;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by developmentpraisindo on 1/14/18.
 */

public class ArenaDaoImpl implements ArenaDao {

    @Override
    public List<ArenaApi> getData() {
        List<ArenaApi> arenas = new ArrayList<>();
        ArenaApi arena1 = new ArenaApi(1, "Snowfort");
        ArenaApi arena2 = new ArenaApi(2, "Omaha");
        ArenaApi arena3 = new ArenaApi(3, "Kaherah");
        arenas.add(arena1);
        arenas.add(arena2);
        arenas.add(arena3);
        return arenas;
    }

}
