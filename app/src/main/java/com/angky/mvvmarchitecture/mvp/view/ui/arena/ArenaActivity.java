package com.angky.mvvmarchitecture.mvp.view.ui.arena;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.angky.mvvmarchitecture.R;
import com.angky.mvvmarchitecture.mvp.model.Arena;
import com.angky.mvvmarchitecture.mvp.presenter.arena.ArenaPresenter;
import com.angky.mvvmarchitecture.mvp.view.ConstantView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

/**
 * Created by pegipegi on 12/12/2017.
 */

public class ArenaActivity extends Activity implements ArenaUseCase {

    @BindView(R.id.progress_bar_arena)
    ProgressBar mProgressBar;
    @BindView(R.id.linear_layout_arena)
    LinearLayout mLinearLayoutArena;
    @BindView(R.id.text_view_result)
    TextView mTextViewResult;
    @BindView(R.id.recycler_view_arena)
    RecyclerView mRecyclerViewArena;

    private ArenaPresenter mArenaPresenter;
    private List<Arena> mData;
    private ArenaAdapter mArenaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arena);
        ButterKnife.bind(this);
        init();
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, ArenaActivity.class);
        context.startActivity(intent);
    }

    @Inject
    public void setmArenaPresenter(ArenaPresenter mArenaPresenter) {
        this.mArenaPresenter = mArenaPresenter;
    }

    private void init() {
        initAdapter();
        loadData();
    }

    private void initAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerViewArena.setLayoutManager(linearLayoutManager);
        mData = new ArrayList<>();
        mArenaAdapter = new ArenaAdapter(mData);
        mRecyclerViewArena.setAdapter(mArenaAdapter);
    }

    private void loadData() {
        mArenaPresenter.loadData();
    }

    @Override
    public void showStatusView(int viewStatus) {
        switch (viewStatus) {
            case ConstantView.VIEW_LOADING:
                mLinearLayoutArena.setVisibility(View.GONE);
                mTextViewResult.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
                break;
            case ConstantView.VIEW_SUCCESS:
                mLinearLayoutArena.setVisibility(View.VISIBLE);
                mTextViewResult.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.GONE);
                break;
            case ConstantView.VIEW_EMPTY:
                mLinearLayoutArena.setVisibility(View.GONE);
                mTextViewResult.setText("The Squad List is Empty");
                mTextViewResult.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
                break;
            case ConstantView.VIEW_ERROR:
                mLinearLayoutArena.setVisibility(View.GONE);
                mTextViewResult.setText("We have tried our best, sorry");
                mTextViewResult.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public void showData(List<Arena> data) {
        this.mData.clear();
        this.mData.addAll(data);
        mArenaAdapter.notifyDataSetChanged();
    }
}
