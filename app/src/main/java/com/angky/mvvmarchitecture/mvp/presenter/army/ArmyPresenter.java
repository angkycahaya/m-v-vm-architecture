package com.angky.mvvmarchitecture.mvp.presenter.army;

/**
 * Created by pegipegi on 12/12/2017.
 *
 */

public interface ArmyPresenter {

    public void loadData();

    public void observeObservable();

    public void observeFlowable();

    public void observeSingle();

    public void observeMaybe();

    public void observeCompletable();
}
