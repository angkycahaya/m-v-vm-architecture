package com.angky.mvvmarchitecture.mvp.presenter.building;

import com.angky.mvvmarchitecture.mvp.data.service.arena.ArenaService;
import com.angky.mvvmarchitecture.mvp.data.service.building.BuildingService;
import com.angky.mvvmarchitecture.mvp.model.ArenaApi;
import com.angky.mvvmarchitecture.mvp.model.Building;
import com.angky.mvvmarchitecture.mvp.model.BuildingApi;
import com.angky.mvvmarchitecture.mvp.view.ConstantView;
import com.angky.mvvmarchitecture.mvp.view.ui.building.BuildingUseCase;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by PEGIPEGI ROG on 1/16/2018.
 */

public class BuildingPresenterImpl implements BuildingPresenter {

    private ArenaService arenaService;
    private BuildingService buildingService;
    private BuildingUseCase buildingUseCase;

    @Inject
    public BuildingPresenterImpl(BuildingUseCase buildingUseCase, ArenaService arenaService, BuildingService buildingService) {
        this.buildingUseCase = buildingUseCase;
        this.arenaService = arenaService;
        this.buildingService = buildingService;
    }

    @Override
    public void loadData() {
        getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getObserver());
    }

    private Observable<List<ArenaApi>> getArenaObservable() {
        return Observable.fromCallable(new Callable<List<ArenaApi>>() {
            @Override
            public List<ArenaApi> call() throws Exception {
                return arenaService.getData();
            }
        });
    }

    private Observable<List<BuildingApi>> getBuildingObservable() {
        return Observable.fromCallable(new Callable<List<BuildingApi>>() {
            @Override
            public List<BuildingApi> call() throws Exception {
                return buildingService.getData();
            }
        });
    }

    private Observable<List<Building>> getObservable() {
        return Observable.zip(getBuildingObservable(), getArenaObservable(),
                new BiFunction<List<BuildingApi>, List<ArenaApi>, List<Building>>() {
                    @Override
                    public List<Building> apply(List<BuildingApi> buildingApis, List<ArenaApi> arenaApis) throws Exception {
                        return BuildingConverter.convertBuildingArena(buildingApis, arenaApis);
                    }
                }
        );
    }

    private Observer<List<Building>> getObserver() {

        return new Observer<List<Building>>() {

            @Override
            public void onSubscribe(Disposable d) {
                buildingUseCase.showStatusView(ConstantView.VIEW_LOADING);
            }

            @Override
            public void onNext(List<Building> buildings) {
                if (buildings.size() > 0) {
                    buildingUseCase.showStatusView(ConstantView.VIEW_SUCCESS);
                    buildingUseCase.showData(buildings);
                } else {
                    buildingUseCase.showStatusView(ConstantView.VIEW_EMPTY);
                }
            }

            @Override
            public void onError(Throwable e) {
                buildingUseCase.showStatusView(ConstantView.VIEW_ERROR);
            }

            @Override
            public void onComplete() {
            }
        };
    }
}
