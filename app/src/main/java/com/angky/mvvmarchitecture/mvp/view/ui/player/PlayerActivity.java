package com.angky.mvvmarchitecture.mvp.view.ui.player;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.angky.mvvmarchitecture.R;
import com.angky.mvvmarchitecture.mvp.model.Player;
import com.angky.mvvmarchitecture.mvp.presenter.player.PlayerPresenter;
import com.angky.mvvmarchitecture.mvp.view.ConstantView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

/**
 * Created by PEGIPEGI ROG on 1/5/2018.
 */

public class PlayerActivity extends Activity implements PlayerUseCase {

    @BindView(R.id.progress_bar_player)
    ProgressBar mProgressBar;
    @BindView(R.id.linear_layout_player)
    LinearLayout mLinearLayoutPlayer;
    @BindView(R.id.text_view_result)
    TextView mTextViewResult;
    @BindView(R.id.recycler_view_player)
    RecyclerView mRecyclerViewPlayer;

    private PlayerPresenter mPlayerPresenter;
    private List<Player> mData;
    private PlayerAdapter mPlayerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        ButterKnife.bind(this);
        init();
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, PlayerActivity.class);
        context.startActivity(intent);
    }

    @Override
    public PlayerActivity getActivity() {
        return this;
    }

    @Inject
    public void setmPlayerPresenter(PlayerPresenter mPlayerPresenter) {
        this.mPlayerPresenter = mPlayerPresenter;
    }

    private void init() {
        initAdapter();
        loadData();
    }

    private void initAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerViewPlayer.setLayoutManager(linearLayoutManager);
        mData = new ArrayList<>();
        mPlayerAdapter = new PlayerAdapter(mData);
        mRecyclerViewPlayer.setAdapter(mPlayerAdapter);
    }

    private void loadData() {
        mPlayerPresenter.loadData();
    }

    @Override
    public void showStatusView(int viewStatus) {
        switch (viewStatus) {
            case ConstantView.VIEW_LOADING:
                mLinearLayoutPlayer.setVisibility(View.GONE);
                mTextViewResult.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
                break;
            case ConstantView.VIEW_SUCCESS:
                mLinearLayoutPlayer.setVisibility(View.VISIBLE);
                mTextViewResult.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.GONE);
                break;
            case ConstantView.VIEW_EMPTY:
                mLinearLayoutPlayer.setVisibility(View.GONE);
                mTextViewResult.setText("The Squad List is Empty");
                mTextViewResult.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
                break;
            case ConstantView.VIEW_ERROR:
                mLinearLayoutPlayer.setVisibility(View.GONE);
                mTextViewResult.setText("We have tried our best, sorry");
                mTextViewResult.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public void showData(List<Player> data) {
        this.mData.clear();
        this.mData.addAll(data);
        mPlayerAdapter.notifyDataSetChanged();
    }
}
