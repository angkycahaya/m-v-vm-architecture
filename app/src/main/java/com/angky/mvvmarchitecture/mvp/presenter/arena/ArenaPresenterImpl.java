package com.angky.mvvmarchitecture.mvp.presenter.arena;

import com.angky.mvvmarchitecture.mvp.data.service.arena.ArenaService;
import com.angky.mvvmarchitecture.mvp.model.Arena;
import com.angky.mvvmarchitecture.mvp.model.ArenaApi;
import com.angky.mvvmarchitecture.mvp.view.ConstantView;
import com.angky.mvvmarchitecture.mvp.view.ui.arena.ArenaUseCase;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by developmentpraisindo on 1/14/18.
 */

public class ArenaPresenterImpl implements ArenaPresenter {

    private ArenaService arenaService;
    private ArenaUseCase arenaUseCase;

    @Inject
    public ArenaPresenterImpl(ArenaUseCase arenaUseCase, ArenaService arenaService) {
        this.arenaUseCase = arenaUseCase;
        this.arenaService = arenaService;
    }

    @Override
    public void loadData() {
        getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        .map(new Function<List<ArenaApi>, List<Arena>>() {

            @Override
            public List<Arena> apply(List<ArenaApi> arenaApis) throws Exception {
                return ArenaConverter.convertListApiToDefault(arenaApis);
            }
        })
        .subscribe(getObserver());
    }

    private Observable<List<ArenaApi>> getObservable() {
        return Observable.fromCallable(new Callable<List<ArenaApi>>() {
            @Override
            public List<ArenaApi> call() throws Exception {
                return arenaService.getData();
            }
        });
    }

    private Observer<List<Arena>> getObserver() {

        return new Observer<List<Arena>>() {

            @Override
            public void onSubscribe(Disposable d) {
                arenaUseCase.showStatusView(ConstantView.VIEW_LOADING);
            }

            @Override
            public void onNext(List<Arena> arenas) {
                if (arenas.size() > 0) {
                    arenaUseCase.showStatusView(ConstantView.VIEW_SUCCESS);
                    arenaUseCase.showData(arenas);
                } else {
                    arenaUseCase.showStatusView(ConstantView.VIEW_EMPTY);
                }
            }

            @Override
            public void onError(Throwable e) {
                arenaUseCase.showStatusView(ConstantView.VIEW_ERROR);
            }

            @Override
            public void onComplete() {
            }
        };
    }

}
