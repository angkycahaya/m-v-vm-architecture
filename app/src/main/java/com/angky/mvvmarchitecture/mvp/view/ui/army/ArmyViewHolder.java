package com.angky.mvvmarchitecture.mvp.view.ui.army;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.angky.mvvmarchitecture.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pegipegi on 12/14/2017.
 *
 */

public class ArmyViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text_view_name)
    TextView mTextViewName;
    @BindView(R.id.text_view_weapon)
    TextView mTextViewWeapon;

    public ArmyViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public TextView getmTextViewName() {
        return mTextViewName;
    }

    public TextView getmTextViewWeapon() {
        return mTextViewWeapon;
    }
}
