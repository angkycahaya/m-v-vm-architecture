package com.angky.mvvmarchitecture.mvp.model;

/**
 * Created by PEGIPEGI ROG on 1/16/2018.
 */

public class BuildingApi {

    private int id;
    private String name;
    private String volume;
    private int arenaId;

    public BuildingApi() {
    }

    public BuildingApi(int id, String name, String volume, int arenaId) {
        this.id = id;
        this.name = name;
        this.volume = volume;
        this.arenaId = arenaId;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public int getArenaId() {
        return arenaId;
    }

    public void setArenaId(int arenaId) {
        this.arenaId = arenaId;
    }
}
