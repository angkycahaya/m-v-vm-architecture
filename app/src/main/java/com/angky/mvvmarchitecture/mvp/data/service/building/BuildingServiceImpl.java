package com.angky.mvvmarchitecture.mvp.data.service.building;

import com.angky.mvvmarchitecture.mvp.data.dao.building.BuildingDao;
import com.angky.mvvmarchitecture.mvp.model.BuildingApi;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 1/16/2018.
 */

public class BuildingServiceImpl implements BuildingService {

    private BuildingDao dao;

    public BuildingServiceImpl(BuildingDao dao) {
        this.dao = dao;
    }

    @Override
    public List<BuildingApi> getData() {
        return dao.getData();
    }
}
