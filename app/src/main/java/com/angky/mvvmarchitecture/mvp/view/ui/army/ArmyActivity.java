package com.angky.mvvmarchitecture.mvp.view.ui.army;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.angky.mvvmarchitecture.R;
import com.angky.mvvmarchitecture.mvp.model.Army;
import com.angky.mvvmarchitecture.mvp.presenter.army.ArmyPresenter;
import com.angky.mvvmarchitecture.mvp.view.ConstantView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

/**
 * Created by pegipegi on 12/12/2017.
 *
 */

public class ArmyActivity extends Activity implements ArmyUseCase {

    @BindView(R.id.progress_bar_army)
    ProgressBar mProgressBar;
    @BindView(R.id.linear_layout_army)
    LinearLayout mLinearLayoutArmy;
    @BindView(R.id.text_view_result)
    TextView mTextViewResult;
    @BindView(R.id.recycler_view_army)
    RecyclerView mRecyclerViewArmy;

    private ArmyPresenter mArmyPresenter;
    private List<Army> mData;
    private ArmyAdapter mArmyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_army);
        ButterKnife.bind(this);
        init();
    }

    @Inject
    public void setmArmyPresenter(ArmyPresenter mArmyPresenter) {
        this.mArmyPresenter = mArmyPresenter;
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, ArmyActivity.class);
        context.startActivity(intent);
    }

    @Override
    public ArmyActivity getActivity() {
        return this;
    }

    private void init() {
        initAdapter();
        loadData();
        mArmyPresenter.observeSingle();
    }

    private void initAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerViewArmy.setLayoutManager(linearLayoutManager);
        mData = new ArrayList<>();
        mArmyAdapter = new ArmyAdapter(mData);
        mRecyclerViewArmy.setAdapter(mArmyAdapter);
    }

    private void loadData() {
        mArmyPresenter.loadData();
    }

    @Override
    public void showStatusView(int viewStatus) {
        switch (viewStatus) {
            case ConstantView.VIEW_LOADING:
                mLinearLayoutArmy.setVisibility(View.GONE);
                mTextViewResult.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
                break;
            case ConstantView.VIEW_SUCCESS:
                mLinearLayoutArmy.setVisibility(View.VISIBLE);
                mTextViewResult.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.GONE);
                break;
            case ConstantView.VIEW_EMPTY:
                mLinearLayoutArmy.setVisibility(View.GONE);
                mTextViewResult.setText("The Squad List is Empty");
                mTextViewResult.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
                break;
            case ConstantView.VIEW_ERROR:
                mLinearLayoutArmy.setVisibility(View.GONE);
                mTextViewResult.setText("We have tried our best, sorry");
                mTextViewResult.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public void showData(List<Army> data) {
        this.mData.clear();
        this.mData.addAll(data);
        mArmyAdapter.notifyDataSetChanged();
    }
}
