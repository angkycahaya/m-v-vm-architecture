package com.angky.mvvmarchitecture.mvp.injector;

import com.angky.mvvmarchitecture.mvp.view.ui.arena.ArenaActivity;
import com.angky.mvvmarchitecture.mvp.view.ui.arena.ArenaActivityModule;
import com.angky.mvvmarchitecture.mvp.view.ui.army.ArmyActivity;
import com.angky.mvvmarchitecture.mvp.view.ui.army.ArmyActivityModule;
import com.angky.mvvmarchitecture.mvp.view.ui.building.BuildingActivity;
import com.angky.mvvmarchitecture.mvp.view.ui.building.BuildingActivityModule;
import com.angky.mvvmarchitecture.mvp.view.ui.main.MainActivity;
import com.angky.mvvmarchitecture.mvp.view.ui.main.MainActivityModule;
import com.angky.mvvmarchitecture.mvp.view.ui.player.PlayerActivity;
import com.angky.mvvmarchitecture.mvp.view.ui.player.PlayerActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by PEGIPEGI ROG on 1/2/2018.
 */

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity bindMainActivity();
    @ContributesAndroidInjector(modules = ArmyActivityModule.class)
    abstract ArmyActivity bindArmyActivity();
    @ContributesAndroidInjector(modules = BuildingActivityModule.class)
    abstract BuildingActivity bindBuildingActivity();
    @ContributesAndroidInjector(modules = PlayerActivityModule.class)
    abstract PlayerActivity bindPlayerActivity();
    @ContributesAndroidInjector(modules = ArenaActivityModule.class)
    abstract ArenaActivity bindArenaActivity();

}
