package com.angky.mvvmarchitecture.mvp.data.dao.building;

import com.angky.mvvmarchitecture.mvp.model.BuildingApi;
import com.angky.mvvmarchitecture.mvp.model.BuildingApi;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PEGIPEGI ROG on 1/16/2018.
 */

public class BuildingDaoImpl implements BuildingDao {

    @Override
    public List<BuildingApi> getData() {
        List<BuildingApi> buildings = new ArrayList<>();
        BuildingApi building1 = new BuildingApi(1, "Aztec", "High", 3);
        BuildingApi building2 = new BuildingApi(2, "Michigan", "Medium", 2);
        BuildingApi building3 = new BuildingApi(3, "Billy The Kid", "Short", 1);
        buildings.add(building1);
        buildings.add(building2);
        buildings.add(building3);
        return buildings;
    }
}
