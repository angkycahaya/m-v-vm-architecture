package com.angky.mvvmarchitecture.mvp.view.ui.arena;

import com.angky.mvvmarchitecture.mvp.model.Arena;

import java.util.List;

/**
 * Created by developmentpraisindo on 1/14/18.
 */

public interface ArenaUseCase {

    public void showStatusView(int viewStatus);

    public void showData(List<Arena> data);
}
