package com.angky.mvvmarchitecture.mvp.view.ui.army;

import com.angky.mvvmarchitecture.mvp.model.Army;

import java.util.List;

/**
 * Created by pegipegi on 12/12/2017.
 *
 */

public interface ArmyUseCase {

    public ArmyActivity getActivity();

    public void showStatusView(int viewStatus);

    public void showData(List<Army> data);

}
