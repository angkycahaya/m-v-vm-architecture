package com.angky.mvvmarchitecture.mvp.listener;

import com.angky.mvvmarchitecture.mvp.model.Player;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 1/5/2018.
 */

public interface OnDataPlayerLoaded {

    public void onLoaded(List<Player> data);
}
