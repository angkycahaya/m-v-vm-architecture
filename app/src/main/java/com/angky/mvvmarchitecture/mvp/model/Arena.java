package com.angky.mvvmarchitecture.mvp.model;

/**
 * Created by developmentpraisindo on 1/14/18.
 */

public class Arena {

    private int id;
    private String name;

    public Arena() {
    }

    public Arena(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
