package com.angky.mvvmarchitecture.mvp.view.ui.building;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.angky.mvvmarchitecture.R;
import com.angky.mvvmarchitecture.mvp.model.Building;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 1/16/2018.
 */

public class BuildingAdapter extends RecyclerView.Adapter<BuildingViewHolder> {

    private List<Building> data;

    public BuildingAdapter(List<Building> data) {
        this.data = data;
    }

    @Override
    public BuildingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_building, parent, false);
        return new BuildingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BuildingViewHolder holder, int position) {
        Building building = data.get(position);
        holder.getmTextViewArena().setText(building.getArena().getName());
        holder.getmTextViewName().setText(building.getName());
        holder.getmTextViewHigh().setText(building.getHigh());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
