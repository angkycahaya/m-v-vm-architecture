package com.angky.mvvmarchitecture.mvp.data.dao.army;

import com.angky.mvvmarchitecture.mvp.listener.OnDataArmyLoaded;

/**
 * Created by pegipegi on 12/12/2017.
 *
 */

public interface ArmyDao {

    public void loadData(OnDataArmyLoaded onDataArmyLoaded);

}
