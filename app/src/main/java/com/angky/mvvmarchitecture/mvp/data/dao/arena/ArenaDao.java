package com.angky.mvvmarchitecture.mvp.data.dao.arena;

import com.angky.mvvmarchitecture.mvp.model.ArenaApi;

import java.util.List;

/**
 * Created by developmentpraisindo on 1/14/18.
 */

public interface ArenaDao {

    public List<ArenaApi> getData();

}
