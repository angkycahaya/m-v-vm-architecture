package com.angky.mvvmarchitecture.mvvm.data.local.dao;

import com.angky.mvvmarchitecture.mvvm.data.local.model.ArenaLocal;
import com.angky.mvvmarchitecture.mvvm.data.local.observer.ArenaLocalObserver;
import com.angky.mvvmarchitecture.mvvm.data.local.observer.ArenaLocalSubscriber;
import com.angky.mvvmarchitecture.mvvm.data.validator.ArenaRequestValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PEGIPEGI ROG on 3/7/2018.
 *
 */

public class ArenaLocalDaoImpl implements ArenaLocalDao {

    private ArenaLocalObserver arenaLocalObserver;
    private ArenaRequestValidator arenaRequestValidator;
    private List<ArenaLocal> data;

    public ArenaLocalDaoImpl(ArenaLocalObserver arenaLocalObserver, ArenaRequestValidator arenaRequestValidator) {
        this.arenaLocalObserver = arenaLocalObserver;
        this.arenaRequestValidator = arenaRequestValidator;
        data = new ArrayList<>();
    }

    @Override
    public void requestData(String param) {
        //run logic to validate param
        String newParam = arenaRequestValidator.validateLocalRequest(param);
        getData(newParam);
    }

    @Override
    public void subscribeArenaLocalObserver(ArenaLocalSubscriber arenaLocalSubscriber) {
        arenaLocalObserver.registerObserver(arenaLocalSubscriber);
    }

    private void publishDataChanged() {
        arenaLocalObserver.publishDataChange(data);
    }

    private void getData(String param) {
        GetData getData = new GetData(param);
        Thread thread = new Thread(getData);
        thread.start();
    }

    private class GetData implements Runnable {

        private String param;

        public GetData(String param) {
            this.param = param;
        }

        @Override
        public void run() {
            // Lets say we already use the param to send request to Local Storage
            try {
                // After processing lets say 1 secon, the local storage return the data
                Thread.sleep(1000);
                data = new ArrayList<>();
                data.add(new ArenaLocal(7, "Indonesia", "Indonesia is featured in Counter-Strike Xbox and reuses textures from Counter-Strike: Condition Zero", 500, 400));
                data.add(new ArenaLocal(8, "Dust", "The map is set in a Middle Eastern town and features two bombsites: one in the Counter-Terrorist spawn and another one in an open area nearby", 700, 400));
                data.add(new ArenaLocal(9, "Library", "Prevent the terrorists bombing the archeological site", 600, 800));
                publishDataChanged();
            } catch (InterruptedException e) {
                data = new ArrayList<>();
            }
        }
    }

}
