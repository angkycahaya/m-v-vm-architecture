package com.angky.mvvmarchitecture.mvvm.data.local.observer;


import com.angky.mvvmarchitecture.mvvm.data.local.model.ArenaLocal;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 2/28/2018.
 *
 */

public interface ArenaLocalSubscriber {

    void notifyDataChangedLocal(List<ArenaLocal> dataArena);

}
