package com.angky.mvvmarchitecture.mvvm.presentation.view.arena;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.angky.mvvmarchitecture.R;
import com.angky.mvvmarchitecture.mvvm.presentation.model.Arena;

import java.util.List;

/**
 * Created by pegipegi on 12/14/2017.
 *
 */

public class ArenaAdapter extends RecyclerView.Adapter<ArenaViewHolder> {

    private List<Arena> mData;

    public ArenaAdapter(List<Arena> mData) {
        this.mData = mData;
    }

    public void setmData(List<Arena> mData) {
        this.mData = mData;
    }

    @Override
    public ArenaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_arena, parent, false);
        return new ArenaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ArenaViewHolder holder, int position) {
        Arena arena = mData.get(position);
        String id = Integer.toString(arena.getId());
        holder.getmTextViewId().setText(id);
        holder.getmTextViewName().setText(arena.getName());
        holder.getmTextViewDescription().setText(arena.getDescription());
        holder.getmTextViewWide().setText(Integer.toString(arena.getWide()));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
