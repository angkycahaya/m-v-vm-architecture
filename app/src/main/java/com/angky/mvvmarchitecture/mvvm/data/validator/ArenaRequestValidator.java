package com.angky.mvvmarchitecture.mvvm.data.validator;

/**
 * Created by PEGIPEGI ROG on 3/9/2018.
 */

public interface ArenaRequestValidator {

    String validateAPIRequest(String param);

    String validateLocalRequest(String param);

}
