package com.angky.mvvmarchitecture.mvvm.data.api.model;

/**
 * Created by developmentpraisindo on 1/14/18.
 *
 */

public class ArenaAPI {

    private int id;
    private String place;
    private String description;
    private int width;
    private int height;

    public ArenaAPI(int id, String place, String description, int width, int height) {
        this.id = id;
        this.place = place;
        this.description = description;
        this.width = width;
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
