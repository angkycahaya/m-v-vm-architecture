package com.angky.mvvmarchitecture.mvvm.domain.factory;

import com.angky.mvvmarchitecture.mvvm.domain.observer.ArenaDomainSubscriber;

/**
 * Created by PEGIPEGI ROG on 3/7/2018.
 *
 */

public interface ArenaFactory {

    void requestData(String param);

    void subscribeArenaDomainObserver(ArenaDomainSubscriber arenaDomainSubscriber);

}
