package com.angky.mvvmarchitecture.mvvm.presentation.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

/**
 * Created by PEGIPEGI ROG on 3/8/2018.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResourceLayout());
        ButterKnife.bind(this);
        onLoad();
        init();
    }

    public void onLoad() {
        initComponent();
        subscribeViewModel();
    }

    public abstract void init();

    public abstract void initComponent();

    public abstract void subscribeViewModel();

    public abstract int getResourceLayout();
}
