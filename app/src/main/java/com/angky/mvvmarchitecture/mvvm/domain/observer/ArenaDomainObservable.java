package com.angky.mvvmarchitecture.mvvm.domain.observer;

import com.angky.mvvmarchitecture.mvvm.presentation.model.Arena;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PEGIPEGI ROG on 2/28/2018.
 *
 */

public class ArenaDomainObservable implements ArenaDomainObserver {

    private List<ArenaDomainSubscriber> subscriberArena = new ArrayList<>();

    @Override
    public void registerObserver(ArenaDomainSubscriber arenaDomainSubscriber) {
        subscriberArena.add(arenaDomainSubscriber);
    }

    @Override
    public void publishDataChange(List<Arena> dataArena) {
        for (ArenaDomainSubscriber subscriber : subscriberArena) {
            subscriber.notifyDataChanged(dataArena);
        }
    }
}
