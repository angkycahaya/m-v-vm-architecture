package com.angky.mvvmarchitecture.mvvm.data.api.observer;

import com.angky.mvvmarchitecture.mvvm.data.api.dao.ArenaAPIDao;
import com.angky.mvvmarchitecture.mvvm.data.api.model.ArenaAPI;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PEGIPEGI ROG on 2/28/2018.
 *
 */

public class ArenaAPIObservable implements ArenaAPIObserver {

    private List<ArenaAPISubscriber> subscriberArena = new ArrayList<>();

    @Override
    public void registerObserver(ArenaAPISubscriber arenaAPISubscriber) {
        subscriberArena.add(arenaAPISubscriber);
    }

    @Override
    public void publishDataChange(List<ArenaAPI> dataArena) {
        for (ArenaAPISubscriber subscriber : subscriberArena) {
            subscriber.notifyDataChangedAPI(dataArena);
        }
    }
}
