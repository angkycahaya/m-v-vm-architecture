package com.angky.mvvmarchitecture.mvvm.presentation.view;

/**
 * Created by pegipegi on 12/12/2017.
 *
 */

public class ConstantView {

    public static final int VIEW_LOADING = 0;
    public static final int VIEW_ERROR = 1;
    public static final int VIEW_EMPTY = 2;
    public static final int VIEW_SUCCESS = 3;
}
