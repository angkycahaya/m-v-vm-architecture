package com.angky.mvvmarchitecture.mvvm.presentation.observer;

import com.angky.mvvmarchitecture.mvvm.presentation.model.Arena;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PEGIPEGI ROG on 2/28/2018.
 *
 */

public class ArenaObservable implements ArenaObserver {

    private List<ArenaSubscriber> subscriberArena = new ArrayList<>();

    @Override
    public void registerObserver(ArenaSubscriber arenaSubscriber) {
        subscriberArena.add(arenaSubscriber);
    }

    @Override
    public void publishDataChange(List<Arena> dataArena) {
        for (ArenaSubscriber subscriber : subscriberArena) {
            subscriber.notifyDataChanged(dataArena);
        }
    }
}
