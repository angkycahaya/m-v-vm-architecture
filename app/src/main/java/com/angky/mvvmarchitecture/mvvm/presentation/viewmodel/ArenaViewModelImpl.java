package com.angky.mvvmarchitecture.mvvm.presentation.viewmodel;

import com.angky.mvvmarchitecture.mvvm.domain.factory.ArenaFactory;
import com.angky.mvvmarchitecture.mvvm.domain.observer.ArenaDomainSubscriber;
import com.angky.mvvmarchitecture.mvvm.presentation.model.Arena;
import com.angky.mvvmarchitecture.mvvm.presentation.observer.ArenaObserver;
import com.angky.mvvmarchitecture.mvvm.presentation.observer.ArenaSubscriber;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 2/28/2018.
 *
 */

public class ArenaViewModelImpl extends ViewModel implements ArenaDomainSubscriber, ArenaViewModel {

    private ArenaObserver mObserver;
    private ArenaFactory mArenaFactory;

    public ArenaViewModelImpl(ArenaObserver mObserver, ArenaFactory mArenaFactory) {
        this.mObserver = mObserver;
        this.mArenaFactory = mArenaFactory;
        init();
    }

    @Override
    public void init() {
        mArenaFactory.subscribeArenaDomainObserver(this);
    }

    @Override
    public void subscribeArenaViewModel(ArenaSubscriber subscriber) {
        mObserver.registerObserver(subscriber);
    }

    @Override
    public void notifyDataChanged(List<Arena> dataArena) {
        mObserver.publishDataChange(dataArena);
    }

    @Override
    public void requestDataChanged(String param) {
        mArenaFactory.requestData(param);
    }
}
