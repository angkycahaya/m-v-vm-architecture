package com.angky.mvvmarchitecture.mvvm.domain.converter;

import com.angky.mvvmarchitecture.mvvm.data.api.model.ArenaAPI;
import com.angky.mvvmarchitecture.mvvm.data.local.model.ArenaLocal;
import com.angky.mvvmarchitecture.mvvm.presentation.model.Arena;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 3/7/2018.
 *
 */

public interface ArenaConverter {

    List<Arena> convertDataFromAPI(List<ArenaAPI> dataAPI);

    List<Arena> convertDataFromLocal(List<ArenaLocal> dataLocal);
}
