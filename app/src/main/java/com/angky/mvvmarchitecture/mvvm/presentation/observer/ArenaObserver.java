package com.angky.mvvmarchitecture.mvvm.presentation.observer;

import com.angky.mvvmarchitecture.mvvm.presentation.model.Arena;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 2/28/2018.
 *
 */
public interface ArenaObserver {

    void registerObserver(ArenaSubscriber arenaSubscriber);

    void publishDataChange(List<Arena> dataArena);

}
