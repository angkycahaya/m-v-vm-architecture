package com.angky.mvvmarchitecture.mvvm.domain.factory;

import com.angky.mvvmarchitecture.mvvm.data.api.dao.ArenaAPIDao;
import com.angky.mvvmarchitecture.mvvm.data.api.model.ArenaAPI;
import com.angky.mvvmarchitecture.mvvm.data.api.observer.ArenaAPISubscriber;
import com.angky.mvvmarchitecture.mvvm.data.local.dao.ArenaLocalDao;
import com.angky.mvvmarchitecture.mvvm.data.local.model.ArenaLocal;
import com.angky.mvvmarchitecture.mvvm.data.local.observer.ArenaLocalSubscriber;
import com.angky.mvvmarchitecture.mvvm.domain.converter.ArenaConverter;
import com.angky.mvvmarchitecture.mvvm.domain.observer.ArenaDomainObserver;
import com.angky.mvvmarchitecture.mvvm.domain.observer.ArenaDomainSubscriber;
import com.angky.mvvmarchitecture.mvvm.presentation.model.Arena;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PEGIPEGI ROG on 3/7/2018.
 *
 */

public class ArenaFactoryImpl implements ArenaFactory, ArenaAPISubscriber, ArenaLocalSubscriber {

    private ArenaDomainObserver arenaDomainObserver;
    private ArenaConverter arenaConverter;
    private ArenaAPIDao arenaAPIDao;
    private ArenaLocalDao arenaLocalDao;
    private List<Arena> dataArenaAPI, dataArenaLocal;

    public ArenaFactoryImpl(ArenaDomainObserver arenaDomainObserver, ArenaConverter arenaConverter, ArenaAPIDao arenaAPIDao, ArenaLocalDao arenaLocalDao) {
        this.arenaDomainObserver = arenaDomainObserver;
        this.arenaConverter = arenaConverter;
        this.arenaAPIDao = arenaAPIDao;
        this.arenaLocalDao = arenaLocalDao;
        dataArenaAPI = new ArrayList<>();
        dataArenaLocal = new ArrayList<>();
        init();
    }

    private void init() {
        arenaAPIDao.subscribeArenaAPIObserver(this);
        arenaLocalDao.subscribeArenaLocalObserver(this);
    }

    @Override
    public void requestData(String param) {
        arenaAPIDao.requestData(param);
        arenaLocalDao.requestData(param);
    }

    @Override
    public void subscribeArenaDomainObserver(ArenaDomainSubscriber arenaDomainSubscriber) {
        arenaDomainObserver.registerObserver(arenaDomainSubscriber);
    }

    private List<Arena> getMergedData() {
        List<Arena> finalData = new ArrayList<>();
        finalData.addAll(dataArenaAPI);
        finalData.addAll(dataArenaLocal);
        return finalData;
    }

    private void publishNewData(List<Arena> dataArena) {
        arenaDomainObserver.publishDataChange(dataArena);
    }

    @Override
    public void notifyDataChangedAPI(List<ArenaAPI> dataArena) {
        dataArenaAPI = arenaConverter.convertDataFromAPI(dataArena);
        List<Arena> mergedData = getMergedData();
        publishNewData(mergedData);
    }

    @Override
    public void notifyDataChangedLocal(List<ArenaLocal> dataArena) {
        dataArenaLocal = arenaConverter.convertDataFromLocal(dataArena);
        List<Arena> mergedData = getMergedData();
        publishNewData(mergedData);
    }
}
