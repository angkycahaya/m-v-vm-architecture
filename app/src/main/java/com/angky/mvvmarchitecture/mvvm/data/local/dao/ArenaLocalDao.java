package com.angky.mvvmarchitecture.mvvm.data.local.dao;

import com.angky.mvvmarchitecture.mvvm.data.local.observer.ArenaLocalSubscriber;

/**
 * Created by PEGIPEGI ROG on 3/7/2018.
 *
 */

public interface ArenaLocalDao {

    void requestData(String param);

    void subscribeArenaLocalObserver(ArenaLocalSubscriber arenaLocalSubscriber);
}
