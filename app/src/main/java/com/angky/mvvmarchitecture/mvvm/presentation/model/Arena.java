package com.angky.mvvmarchitecture.mvvm.presentation.model;

/**
 * Created by developmentpraisindo on 1/14/18.
 */

public class Arena {

    private int id;
    private String name;
    private String description;
    private int wide;

    public Arena() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWide() {
        return wide;
    }

    public void setWide(int wide) {
        this.wide = wide;
    }
}
