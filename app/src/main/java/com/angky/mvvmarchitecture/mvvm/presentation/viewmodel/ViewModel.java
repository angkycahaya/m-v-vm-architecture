package com.angky.mvvmarchitecture.mvvm.presentation.viewmodel;

/**
 * Created by PEGIPEGI ROG on 3/7/2018.
 */

public abstract class ViewModel {

    public abstract void init();

}
