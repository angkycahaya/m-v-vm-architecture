package com.angky.mvvmarchitecture.mvvm.data.local.observer;

import com.angky.mvvmarchitecture.mvvm.data.local.dao.ArenaLocalDao;
import com.angky.mvvmarchitecture.mvvm.data.local.model.ArenaLocal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PEGIPEGI ROG on 2/28/2018.
 *
 */

public class ArenaLocalObservable implements ArenaLocalObserver {

    private List<ArenaLocalSubscriber> subscriberArena = new ArrayList<>();

    @Override
    public void registerObserver(ArenaLocalSubscriber arenaLocalSubscriber) {
        subscriberArena.add(arenaLocalSubscriber);
    }

    @Override
    public void publishDataChange(List<ArenaLocal> dataArena) {
        for (ArenaLocalSubscriber subscriber : subscriberArena) {
            subscriber.notifyDataChangedLocal(dataArena);
        }
    }
}
