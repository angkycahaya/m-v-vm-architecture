package com.angky.mvvmarchitecture.mvvm.data.api.dao;

import com.angky.mvvmarchitecture.mvvm.data.api.model.ArenaAPI;
import com.angky.mvvmarchitecture.mvvm.data.api.observer.ArenaAPIObserver;
import com.angky.mvvmarchitecture.mvvm.data.api.observer.ArenaAPISubscriber;
import com.angky.mvvmarchitecture.mvvm.data.validator.ArenaRequestValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PEGIPEGI ROG on 3/7/2018.
 *
 */

public class ArenaAPIDaoImpl implements ArenaAPIDao {

    private ArenaAPIObserver arenaAPIObserver;
    private ArenaRequestValidator arenaRequestValidator;
    private List<ArenaAPI> data;

    public ArenaAPIDaoImpl(ArenaAPIObserver arenaAPIObserver, ArenaRequestValidator arenaRequestValidator) {
        this.arenaAPIObserver = arenaAPIObserver;
        this.arenaRequestValidator = arenaRequestValidator;
        data = new ArrayList<>();
    }

    @Override
    public void requestData(String param) {
        //run logic to validate param
        String newParam = arenaRequestValidator.validateLocalRequest(param);
        getData(newParam);
    }

    @Override
    public void subscribeArenaAPIObserver(ArenaAPISubscriber arenaAPISubscriber) {
        arenaAPIObserver.registerObserver(arenaAPISubscriber);
    }

    private void publishDataChanged() {
        arenaAPIObserver.publishDataChange(data);
    }

    private void getData(String param) {
        GetData getData = new GetData(param);
        Thread thread = new Thread(getData);
        thread.start();
    }

    private class GetData implements Runnable {

        private String param;

        public GetData(String param) {
            this.param = param;
        }

        @Override
        public void run() {
            // Lets say we already use the param to send request to Local Storage
            try {
                // After processing lets say 5 secon, the local storage return the data
                Thread.sleep(5000);
                data = new ArrayList<>();
                data.add(new ArenaAPI(1, "Aztec", "Bomb defusal map featured in the Counter-Strike series", 500, 400));
                data.add(new ArenaAPI(2, "Philadelphia", "It is set in a Central American archaeological site", 700, 400));
                data.add(new ArenaAPI(3, "Mangroot", "Prevent the terrorists bombing the archeological site", 600, 800));
                data.add(new ArenaAPI(4, "Italy", "Destroy the valuable Italy ruins", 500, 400));
                data.add(new ArenaAPI(5, "Rome", "Roma in Counter-Strike 1.5 had some dust textures", 700, 400));
                data.add(new ArenaAPI(6, "France", "The bridge was altered to some extent and textures for the entire map were improved", 600, 800));
                publishDataChanged();
            } catch (InterruptedException e) {
                data = new ArrayList<>();
            }
        }
    }

}
