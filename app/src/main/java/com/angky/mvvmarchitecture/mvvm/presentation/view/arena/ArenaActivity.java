package com.angky.mvvmarchitecture.mvvm.presentation.view.arena;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.angky.mvvmarchitecture.R;
import com.angky.mvvmarchitecture.mvvm.data.api.dao.ArenaAPIDao;
import com.angky.mvvmarchitecture.mvvm.data.api.dao.ArenaAPIDaoImpl;
import com.angky.mvvmarchitecture.mvvm.data.api.observer.ArenaAPIObservable;
import com.angky.mvvmarchitecture.mvvm.data.api.observer.ArenaAPIObserver;
import com.angky.mvvmarchitecture.mvvm.data.local.dao.ArenaLocalDao;
import com.angky.mvvmarchitecture.mvvm.data.local.dao.ArenaLocalDaoImpl;
import com.angky.mvvmarchitecture.mvvm.data.local.observer.ArenaLocalObservable;
import com.angky.mvvmarchitecture.mvvm.data.local.observer.ArenaLocalObserver;
import com.angky.mvvmarchitecture.mvvm.data.validator.ArenaRequestValidator;
import com.angky.mvvmarchitecture.mvvm.data.validator.ArenaRequestValidatorImpl;
import com.angky.mvvmarchitecture.mvvm.domain.converter.ArenaConverter;
import com.angky.mvvmarchitecture.mvvm.domain.converter.ArenaConverterImpl;
import com.angky.mvvmarchitecture.mvvm.domain.factory.ArenaFactory;
import com.angky.mvvmarchitecture.mvvm.domain.factory.ArenaFactoryImpl;
import com.angky.mvvmarchitecture.mvvm.domain.observer.ArenaDomainObservable;
import com.angky.mvvmarchitecture.mvvm.domain.observer.ArenaDomainObserver;
import com.angky.mvvmarchitecture.mvvm.presentation.model.Arena;
import com.angky.mvvmarchitecture.mvvm.presentation.observer.ArenaObservable;
import com.angky.mvvmarchitecture.mvvm.presentation.observer.ArenaObserver;
import com.angky.mvvmarchitecture.mvvm.presentation.observer.ArenaSubscriber;
import com.angky.mvvmarchitecture.mvvm.presentation.view.BaseActivity;
import com.angky.mvvmarchitecture.mvvm.presentation.view.ConstantView;
import com.angky.mvvmarchitecture.mvvm.presentation.viewmodel.ArenaViewModel;
import com.angky.mvvmarchitecture.mvvm.presentation.viewmodel.ArenaViewModelImpl;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by pegipegi on 12/12/2017.
 *
 */

public class ArenaActivity extends BaseActivity implements ArenaSubscriber {

    @BindView(R.id.progress_bar_arena)
    ProgressBar mProgressBar;
    @BindView(R.id.linear_layout_arena)
    LinearLayout mLinearLayoutArena;
    @BindView(R.id.text_view_result)
    TextView mTextViewResult;
    @BindView(R.id.recycler_view_arena)
    RecyclerView mRecyclerViewArena;

    private ArenaViewModel mArenaViewModel;
    private ArenaAdapter mArenaAdapter;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, ArenaActivity.class);
        context.startActivity(intent);
    }

    @Override
    public int getResourceLayout() {
        return R.layout.activity_arena;
    }

    @Override
    public void init() {
        initAdapter();
        loadData();
    }

    @Override
    public void initComponent() {
        //data layer
        ArenaRequestValidator arenaRequestValidator = new ArenaRequestValidatorImpl();
        ArenaAPIObserver arenaAPIObserver = new ArenaAPIObservable();
        ArenaAPIDao arenaAPIDao = new ArenaAPIDaoImpl(arenaAPIObserver, arenaRequestValidator);
        ArenaLocalObserver arenaLocalObserver = new ArenaLocalObservable();
        ArenaLocalDao arenaLocalDao = new ArenaLocalDaoImpl(arenaLocalObserver, arenaRequestValidator);
        //data converter
        ArenaConverter arenaConverter = new ArenaConverterImpl();
        //Observer to domain
        ArenaDomainObserver arenaDomainObserver = new ArenaDomainObservable();
        //init factory
        ArenaFactory arenaFactory = new ArenaFactoryImpl(arenaDomainObserver, arenaConverter, arenaAPIDao, arenaLocalDao);
        //Observer to view model
        ArenaObserver arenaObserver = new ArenaObservable();
        this.mArenaViewModel = new ArenaViewModelImpl(arenaObserver, arenaFactory);
    }

    @Override
    public void subscribeViewModel() {
        mArenaViewModel.subscribeArenaViewModel(this);
    }

    private void initAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerViewArena.setLayoutManager(linearLayoutManager);
        mArenaAdapter = new ArenaAdapter(new ArrayList<Arena>());
        mRecyclerViewArena.setAdapter(mArenaAdapter);
    }

    private void loadData() {
        showStatusView(ConstantView.VIEW_LOADING);
        // Lets say we have some param taken from view to be passed
        // and sent along with the request called
        String param = "X-196AL956";
        mArenaViewModel.requestDataChanged(param);
    }

    private void showStatusView(int viewStatus) {
        switch (viewStatus) {
            case ConstantView.VIEW_LOADING:
                mLinearLayoutArena.setVisibility(View.GONE);
                mTextViewResult.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
                break;
            case ConstantView.VIEW_SUCCESS:
                mLinearLayoutArena.setVisibility(View.VISIBLE);
                mTextViewResult.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.GONE);
                break;
            case ConstantView.VIEW_EMPTY:
                mLinearLayoutArena.setVisibility(View.GONE);
                mTextViewResult.setText(getString(R.string.msg_global_no_data));
                mTextViewResult.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
                break;
            case ConstantView.VIEW_ERROR:
                mLinearLayoutArena.setVisibility(View.GONE);
                mTextViewResult.setText(getString(R.string.msg_global_error));
                mTextViewResult.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    private void showData(List<Arena> dataArena) {
        try {
            if (dataArena != null && dataArena.size() > 0) {
                showStatusView(ConstantView.VIEW_SUCCESS);
                mArenaAdapter.setmData(dataArena);
                mArenaAdapter.notifyDataSetChanged();
            } else {
                showStatusView(ConstantView.VIEW_EMPTY);
            }
        } catch (IllegalStateException ise) {
            showStatusView(ConstantView.VIEW_ERROR);
        }
    }

    @Override
    public void notifyDataChanged(final List<Arena> dataArena) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showData(dataArena);
            }
        });
    }

}
