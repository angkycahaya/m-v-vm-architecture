package com.angky.mvvmarchitecture.mvvm.data.validator;

/**
 * Created by PEGIPEGI ROG on 3/9/2018.
 *
 */

public class ArenaRequestValidatorImpl implements ArenaRequestValidator {

    private String validateRequest(String param) {
        //run validate logic here
        String result = param;
        result = result.replace("'", "");
        result = result.replace("\"", "");
        result = result.replace("\\", "");
        return result;
    }

    @Override
    public String validateAPIRequest(String param) {
        return validateRequest(param);
    }

    @Override
    public String validateLocalRequest(String param) {
        return validateRequest(param);
    }
}
