package com.angky.mvvmarchitecture.mvvm.domain.observer;

import com.angky.mvvmarchitecture.mvvm.presentation.model.Arena;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 2/28/2018.
 *
 */

public interface ArenaDomainObserver {

    void registerObserver(ArenaDomainSubscriber arenaDomainSubscriber);

    void publishDataChange(List<Arena> dataArena);

}
