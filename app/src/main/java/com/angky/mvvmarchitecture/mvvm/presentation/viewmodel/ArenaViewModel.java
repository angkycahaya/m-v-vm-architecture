package com.angky.mvvmarchitecture.mvvm.presentation.viewmodel;

import com.angky.mvvmarchitecture.mvvm.presentation.model.Arena;
import com.angky.mvvmarchitecture.mvvm.presentation.observer.ArenaSubscriber;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 2/28/2018.
 *
 */

public interface ArenaViewModel {

    void subscribeArenaViewModel(ArenaSubscriber subscriber);

    void notifyDataChanged(List<Arena> dataArena);

    void requestDataChanged(String param);

}
