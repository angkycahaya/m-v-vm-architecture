package com.angky.mvvmarchitecture.mvvm.domain.converter;

import com.angky.mvvmarchitecture.mvvm.data.api.model.ArenaAPI;
import com.angky.mvvmarchitecture.mvvm.data.local.model.ArenaLocal;
import com.angky.mvvmarchitecture.mvvm.presentation.model.Arena;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PEGIPEGI ROG on 3/7/2018.
 *
 */

public class ArenaConverterImpl implements ArenaConverter {

    private Arena convertModelFromAPI(ArenaAPI arenaAPI) {
        Arena arena = new Arena();
        arena.setId(arenaAPI.getId());
        arena.setName(arenaAPI.getPlace());
        arena.setDescription(arenaAPI.getDescription());
        int wide = arenaAPI.getWidth() * arenaAPI.getHeight();
        arena.setWide(wide);
        return arena;
    }

    private Arena convertModelFromLocal(ArenaLocal arenaLocal) {
        Arena arena = new Arena();
        arena.setId(arenaLocal.getCode());
        arena.setName(arenaLocal.getPlace());
        arena.setDescription(arenaLocal.getDescription());
        int wide = arenaLocal.getWidth() * arenaLocal.getHeight();
        arena.setWide(wide);
        return arena;
    }

    @Override
    public List<Arena> convertDataFromAPI(List<ArenaAPI> dataAPI) {
        List<Arena> result = new ArrayList<>();
        for (ArenaAPI data: dataAPI) {
            result.add(convertModelFromAPI(data));
        }
        return result;
    }

    @Override
    public List<Arena> convertDataFromLocal(List<ArenaLocal> dataLocal) {
        List<Arena> result = new ArrayList<>();
        for (ArenaLocal data: dataLocal) {
            result.add(convertModelFromLocal(data));
        }
        return result;
    }
}
