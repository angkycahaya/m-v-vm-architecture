package com.angky.mvvmarchitecture.mvvm.data.api.dao;

import com.angky.mvvmarchitecture.mvvm.data.api.observer.ArenaAPISubscriber;

/**
 * Created by PEGIPEGI ROG on 3/7/2018.
 *
 */

public interface ArenaAPIDao {

    void requestData(String param);

    void subscribeArenaAPIObserver(ArenaAPISubscriber arenaAPISubscriber);
}
