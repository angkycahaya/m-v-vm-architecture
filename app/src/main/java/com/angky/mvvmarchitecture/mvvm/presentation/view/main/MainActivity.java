package com.angky.mvvmarchitecture.mvvm.presentation.view.main;

import android.os.Bundle;
import android.widget.Button;

import com.angky.mvvmarchitecture.R;
import com.angky.mvvmarchitecture.mvvm.presentation.view.BaseActivity;
import com.angky.mvvmarchitecture.mvvm.presentation.view.arena.ArenaActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.button_arenas)
    Button mButtonMVPArenas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    public void init() {
    }

    @Override
    public void initComponent() {
    }

    @Override
    public void subscribeViewModel() {
    }

    @Override
    public int getResourceLayout() {
        return R.layout.activity_main;
    }

    @OnClick(R.id.button_arenas)
    public void onButtonMVPArenaClicked() {
        ArenaActivity.startActivity(this);
    }

}
