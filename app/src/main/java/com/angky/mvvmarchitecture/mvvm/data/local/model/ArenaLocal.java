package com.angky.mvvmarchitecture.mvvm.data.local.model;

/**
 * Created by developmentpraisindo on 1/14/18.
 *
 */

public class ArenaLocal {

    private int code;
    private String place;
    private String description;
    private int width;
    private int height;

    public ArenaLocal(int code, String place, String description, int width, int height) {
        this.code = code;
        this.place = place;
        this.description = description;
        this.width = width;
        this.height = height;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
