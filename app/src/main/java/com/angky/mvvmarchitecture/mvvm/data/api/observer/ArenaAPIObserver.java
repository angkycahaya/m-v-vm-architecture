package com.angky.mvvmarchitecture.mvvm.data.api.observer;

import com.angky.mvvmarchitecture.mvvm.data.api.model.ArenaAPI;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 2/28/2018.
 *
 */

public interface ArenaAPIObserver {

    void registerObserver(ArenaAPISubscriber arenaAPISubscriber);

    void publishDataChange(List<ArenaAPI> dataArena);

}
